var bodyParser = require("body-parser");
var express = require('express');
var path = require('path');
var request = require('request');
var fs = require('fs');
var serveStatic = require('serve-static');
var Multer = require('multer');
var multer = Multer({storage: Multer.MemoryStorage});
var app = express();
var port = process.env.PORT || 8080;
var folder;
var paths = [];

const Storage = require('@google-cloud/storage');
const bucketName = 'mlfc-initial';
const bucketName2 = 'mlfc';
const bucketName3 = 'mlfc-checkin'
const homedir = require('os').homedir();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); //allows app to parse json and urlencoded bodies of http requests

app.use(express.static(__dirname));

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
  sendUploadPost(); //retrieve data directory in case of uploading files
})//listen

app.get('/', (req, res) => {
  res.send("hello");

  res.sendFile(path.join(__dirname + '/index.html')); //send html page to user
})//get

app.get('/style.css', function(req, res) {
  res.sendFile(__dirname + "/style.css"); //send css to user

})//get

//Post call to handle uploading non-annotated files
app.post('/files', multer.array('files'), function (req, res) { //multer used to obtain files in req.files
    console.log(req.files)
    for (var file of req.files) {
        sendUploadToGCS(file, folder.folder);
    };
    sendUploadPost(); //prepare next directory to upload to
    res.send('Upload successful');
})//post

//take local file and send to google cloud bucket
function sendUploadToGCS(reqFile, dir) {
  const storage = new Storage();
  const gcsname = reqFile.originalname; //get name of local file
  const bucket = storage.bucket(bucketName); //get bucket to upload to
  const file = bucket.file(dir + gcsname); //get filet object to upload to
  const stream = file.createWriteStream({
    metadata: {
      contentType: reqFile.mimetype
    },
    resumable: false
  }); //create write stream to write local file to file on bucket
  stream.end(reqFile.buffer); //send file content through stream
}//sendUploadToGCS

//send post to will's app to return directories to upload files to
function sendUploadPost(){
   var clientServerOptions = {
                uri: "https://mlfc-api-dot-oasis-build-747.appspot.com/upload",
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            request(clientServerOptions, function (error, response) {
              folder = JSON.parse(response.body);
              console.log("error: " + error);
              //console.log("data: " + response.body)
              console.log(folder.folder);
              return;
            });
}//sendUploadPost

//Post call to handle uploading annotated files
app.post('/upload/annotated', multer.array('files'), function (req, res) { //multer used to obtain files in req.files
    console.log("filename: "+req.files.filename)
    for (var file of req.files) {
        sendAnnotatedUploadToGCS(file, folder.folder);
    };
    sendAnnotatedUploadPost(); //prepare next directory to upload to
    res.send('Upload successful');
})//post

//take local file and send to google cloud bucket
function sendAnnotatedUploadToGCS(reqFile, dir) {
  const storage = new Storage();
  const gcsname = reqFile.originalname; //get name of local file
  const bucket = storage.bucket(bucketName3); //get bucket to upload to
  const file = bucket.file(dir + gcsname); //get filet object to upload to
  const stream = file.createWriteStream({
    metadata: {
      contentType: reqFile.mimetype
    },
    resumable: false
  }); //create write stream to write local file to file on bucket
  stream.end(reqFile.buffer); //send file content through stream
}//sendAnnotatedUploadToGCS

function sendAnnotatedUploadPost(){
   var clientServerOptions = {
                uri: "https://mlfc-api-dot-oasis-build-747.appspot.com/checkin",
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            request(clientServerOptions, function (error, response) {
              folder = JSON.parse(response.body)
              console.log(folder.folder)
              return;
            });
}//sendAnnotatedUploadPost

//handle a post call to checkout route
app.post('/checkout', function checkout(req, res) {
  const storage = new Storage();
  var num = req.headers.files; //number of files user requested
  var type = req.headers.filetype;
  console.log("NumFiles index.js: "+num);
  request.get({ //make get request to will's appspot to receive names of files to download
    url: "https://mlfc-api-dot-oasis-build-747.appspot.com/checkout",
    headers: {
      'files': num, //format expected by will's api
      'filetype': type
    }
  }, (err, response, body) => {
    responseCheck=response.body;
    res.send(responseCheck);
    })
})//post

//handle a post call to checkoutXML route
app.post('/checkoutxml', function checkout(req, res) {
  const storage = new Storage();
  var num = req.headers.files;//number of files user requested
  var type= req.headers.filetype;
  var downloadSpec=req.headers.downloadspec;
  console.log("FileType index.js: "+type);
  console.log(JSON.stringify(req.headers));
  request.get({ //make get request to will's appspot to receive names of files to download
    url: "https://mlfc-api-dot-oasis-build-747.appspot.com/checkoutxml",
    headers: {
      'filetype': type,
      'files': num,
      'downloadspec':downloadSpec//format expected by will's api
    }
  }, (err, response, body) => {

    res.send(response.body);
  })
})//post
